#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct SocketItem {
    int socket;
    struct SocketItem *next;
} SocketItem;

SocketItem *sockets_list_create(int, SocketItem *);

SocketItem *sockets_list_append(SocketItem *, int);

SocketItem *sockets_list_at(SocketItem *, int);

SocketItem *sockets_list_remove(SocketItem *, int);

void sockets_list_foreach(SocketItem *head, void (*func)(int));