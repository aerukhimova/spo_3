#include "linked_socket_list.h"

SocketItem *sockets_list_create(int socket, SocketItem *next) {
    SocketItem *new_node = (SocketItem *) malloc(sizeof(SocketItem));
    if (new_node == NULL) {
        printf("Ошибка!\n");
        exit(0);
    }
    new_node->socket = socket;
    new_node->next = next;
    return new_node;
}

SocketItem *sockets_list_append(SocketItem *head, int value) {
    SocketItem *cursor = head;

    if (head == NULL) return sockets_list_create(value, head);
    while (cursor->next != NULL) cursor = cursor->next;
    cursor->next = sockets_list_create(value, NULL);
    return head;
}

int sockets_list_length(SocketItem *head) {
    SocketItem *cursor = head;
    int count = 0;
    while (cursor != NULL) {
        count++;
        cursor = cursor->next;
    }
    return count;
}

SocketItem *sockets_list_at(SocketItem *head, int index) {
    SocketItem *cursor = head;

    int i = 0;
    if (sockets_list_length(head) < index || index < 0) return NULL;
    while (i < index) {
        cursor = cursor->next;
        i++;
    }
    return cursor;
}

SocketItem *sockets_list_remove(SocketItem *head, int socket) {
    SocketItem *cursor = head;
    SocketItem *cursor_prev = NULL;
    int index = 0;
    while (cursor != NULL) {
        if (cursor->socket == socket) {
            if (index == 0) {
                SocketItem *tmp = cursor->next;
                free(cursor);
                return tmp;
            } else {
                SocketItem *tmp = cursor->next;
                cursor_prev->next = tmp;
                free(cursor);
                return head;
            }
        }
        cursor_prev = cursor;
        cursor = cursor->next;
        index++;
    }
    return head;
}

void sockets_list_foreach(SocketItem *head, void (*func)(int)) {
    int len = sockets_list_length(head);
    int i;
    for (i = 0; i < len; i++)func(sockets_list_at(head, i)->socket);
}
