#include "linked_files_list.h"

ListItem *list_create(ListItem *next, char *name, char *type, char *date, u_long size) {

    ListItem *new_node = (ListItem *) malloc(sizeof(ListItem));
    if (new_node == NULL) exit(1);

    new_node->file = *(File *) malloc(sizeof(File));
    new_node->file.size = size;

    new_node->file.type = malloc(strlen(type) * sizeof(char));
    memset(new_node->file.type, 0, sizeof(*new_node->file.type));
    strcpy(new_node->file.type, type);

    new_node->file.name = malloc(255 * sizeof(char));
    memset(new_node->file.name, 0, sizeof(*new_node->file.name));
    strcpy(new_node->file.name, name);

    new_node->file.date = malloc(strlen(date) * sizeof(char));
    memset(new_node->file.date, 0, sizeof(*new_node->file.date));
    strcpy(new_node->file.date, date);

    new_node->next = next;
    return new_node;
}

ListItem *list_append(ListItem *head, char *name, char *type, char *date, u_long size) {
    ListItem *cursor = head;

    if (head == NULL) return list_create(head, name, type, date, size);
    while (cursor->next != NULL) cursor = cursor->next;
    cursor->next = list_create(NULL, name, type, date, size);
    return head;
}

ListItem *list_at(ListItem *head, int index) {
    ListItem *cursor = head;
    int i = 0;
    if (list_size(head) < index || index < 0) return NULL;
    while (i < index) {
        cursor = cursor->next;
        i++;
    }
    return cursor;
}

int list_size(ListItem *head) {
    ListItem *cursor = head;
    int count = 0;
    while (cursor != NULL) {
        count++;
        cursor = cursor->next;
    }
    return count;
}

ListItem *list_clear(ListItem *node) {
    ListItem *nodeForFree;
    while (node != NULL) {
        nodeForFree = node;
        node = node->next;
        free(nodeForFree);
    }
    return node;
}
