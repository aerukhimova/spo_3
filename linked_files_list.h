#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "ui.h"

typedef struct ListItem {
    File file;
    struct ListItem *next;
} ListItem;

ListItem *list_append(ListItem *head, char *name,
                      char *type,
                      char *date,
                      u_long size);

int list_size(ListItem *);

ListItem *list_at(ListItem *head, int index);

ListItem *list_clear(ListItem *node);

