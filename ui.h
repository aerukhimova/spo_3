#pragma once

#include <string.h>
#include <ncurses.h>
#include <locale.h>
#include <stdlib.h>
#include <zconf.h>
#include "utils.h"

extern bool view_mode;

typedef struct File {
    char *type;
    char *name;
    u_long size;
    char *date;
} File;

void init_file_wrap();

void print_files_info();

void append_file_info(File *file);

void init_ui();

void start_read_key(void (*on_select)(char *, bool), void (*on_download)(char *), void (*on_upload)(char *));

void init_file_view();