#include <stdlib.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <zconf.h>
#include <string.h>
#include "utils.h"
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "linked_socket_list.h"
#include <sys/event.h>

int start_server(unsigned int port, char *path);

