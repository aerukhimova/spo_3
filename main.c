#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client.h"
#include "server.h"
#include "ui.h"

void printHelp(char *argv[]) {
    printf("Usage:\n"
           "\t%s server [port] [working_dir]    - Запустить сервер на [port] порту в директории working_dir\n"
           "\t%s client [port] [address]        - Подкючиться к серверу по адресу address:port\n",
           argv[0], argv[0]);
}

int main(int argc, char *argv[]) {
    if (argc == 4) {
        int port = atoi(argv[2]);
        if (port <= 0) {
            puts("Порт должен быть больше 0");
            exit(1);
        }

        if (strcmp(argv[1], "server") == 0) {
            if (!file_exists(argv[3])) {
                puts("working_dir не существует!");
                exit(1);
            }
            if (!is_directory(argv[3])) {
                puts("working_dir не директория!");
                exit(1);
            }
            return start_server(port, argv[3]);
        } else if (strcmp(argv[1], "client") == 0) {
            init_ui();
            return start_client(argv[3], port);
        } else printHelp(argv);
    } else printHelp(argv);


    return 0;
}
