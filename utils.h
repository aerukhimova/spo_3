#pragma once

#include <zconf.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>

#define TEMP_FILE               "/tmp/tmp_open_file"

#define REQUEST_BODY_OFFSET     10
#define RESPONSE_BODY_OFFSET    10

#define FOREACH_STATUS(STATUS) \
        STATUS(SUCCESS)   \
        STATUS(UPDATE)   \
        STATUS(ERROR)  \

#define FOREACH_COMMAND(COMMAND) \
        COMMAND(GET_LIST)   \
        COMMAND(GET_FILE)  \
        COMMAND(UPLOAD)  \
        COMMAND(UNDEFINED)  \

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

typedef enum Command {
    FOREACH_COMMAND(GENERATE_ENUM)
} Command;

typedef enum Status {
    FOREACH_STATUS(GENERATE_ENUM)
} Status;

static const char *COMMAND[] = {FOREACH_COMMAND(GENERATE_STRING)};
static const char *STATUS[] = {FOREACH_STATUS(GENERATE_STRING)};

Command parse_command(char *command_str);

Status parse_status(char *status_str);

char *trim(char *str);

void replace_string(char **result, const char *s, const char *oldW, const char *newW);

bool is_directory(const char *path);

bool file_exists(char *filename);
